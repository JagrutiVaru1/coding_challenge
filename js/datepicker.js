const alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "W", "X", "Y", "Z"];
const numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

var counter = 1;

function addNewPiker() {
    var newPiker = document.createElement('div');
    newPiker.innerHTML = '<div class="card bg-light mb-4 picker-header text-center mr-5" >' +
        '<input class="w-100 mainInputContainer" id="final-input-' + counter + '" onfocus=hideShowPikerBody("picker-body-' + counter + '")></input>' +
        '<div class="card-body d-none text-center" id="picker-body-' + counter + '">' +
        '<input type="text" class="w-100" id="picker-name-' + counter + '">' +
        '<div class="d-flex justify-content-around pt-2">' +
        '<div>' +
        '<i class="fa fa-angle-up cursorPointer" aria-hidden="true" onClick=upArrowForAlphabet("alphabet-' + counter + '")></i>' +
        '<p class="m-0" id="alphabet-' + counter + '">A</p>' +
        '<i class="fa fa-angle-down cursorPointer" aria-hidden="true" onClick=downArrowForAlphabet("alphabet-' + counter + '")></i>' +
        '</div>' +
        '<div>' +
        '<i class="fa fa-angle-up cursorPointer" aria-hidden="true" onClick=upArrowDigit("number-' + counter + '")></i>' +
        '<p class="m-0" id="number-' + counter + '">0</p>' +
        '<i class="fa fa-angle-down cursorPointer" aria-hidden="true" onClick=downArrowDigit("number-' + counter + '")></i>' +
        '</div>' +
        '</div>' +
        '<button onclick=done(' + counter + ') class="text-uppercase">done</button>' +
        '</div>' +
        '</div>';

    counter += 1;
    document.getElementById("mainDynamicContainer").appendChild(newPiker);
}

function hideShowPikerBody(id) {
    var element = document.getElementById(id);
    element.classList.toggle("d-none");
}

function upArrowForAlphabet(id) {
    var element = document.getElementById(id);
    var currentAlphabetsIndex = alphabets.indexOf(element.textContent);
    if ((currentAlphabetsIndex + 1) < alphabets.length) {
        element.textContent = alphabets[currentAlphabetsIndex + 1];
    } else {
        element.textContent = alphabets[0];
    }
}

function downArrowForAlphabet(id) {
    var element = document.getElementById(id);
    var currentAlphabetsIndex = alphabets.indexOf(element.textContent);
    if (currentAlphabetsIndex > 0) {
        element.textContent = alphabets[currentAlphabetsIndex - 1];
    } else {
        element.textContent = alphabets[alphabets.length - 1];
    }
}

function upArrowDigit(id) {
    var element = document.getElementById(id);
    var currentNumberIndex =  numbers.indexOf(element.textContent);
    if ((currentNumberIndex + 1) < numbers.length) {
        element.textContent = numbers[(currentNumberIndex + 1)];
    } else {
        element.textContent = numbers[0];
    }
}

function downArrowDigit(id) {
    var element = document.getElementById(id);
    var currentNumber = numbers.indexOf(element.textContent);
    if (currentNumber > 0) {
        element.textContent = numbers[(currentNumber - 1)];
    } else {
        element.textContent = numbers[numbers.length - 1];
    }
}

function done(counter) {
    var nameVale = document.getElementById("picker-name-" + counter);
    var mainInput = document.getElementById("final-input-" + counter);
    var alphabet = document.getElementById("alphabet-" + counter).textContent;
    var number = document.getElementById("number-" + counter).textContent;
    mainInput.value = nameVale.value + ' * ' + alphabet + ' * ' + number;
    var element = document.getElementById("picker-body-" + counter);
    element.classList.toggle("d-none");
}